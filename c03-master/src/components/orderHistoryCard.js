// import {useState} from 'react'
import {Card, Row, Col} from 'react-bootstrap'
import {Link} from 'react-router-dom'


export default function OrderCard({orderProp}) {

console.log(orderProp)

// object destructuring 
const {productId, quantity} = orderProp


    return (
     <Row xs={1} md={2} className="g-4">
     {Array.from({ length: 2 }).map((_, idx) => (
        <Col>
            <Card border="dark">
               
                <Card.Body>
                <Card.Title>{productId}</Card.Title>
                <Card.Subtitle>Quantiy{quantity}</Card.Subtitle>
                <Card.Text>
                This is a longer card with supporting text below as a natural
                lead-in to additional content. This content is a little bit longer.
                </Card.Text>
                
                </Card.Body>
            </Card>
        </Col>
        ))}
     </Row>





    )
}
