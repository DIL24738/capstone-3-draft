import {Fragment, useEffect, useState} from 'react'
import OrderCard from '../components/orderHistoryCard'
import {Table} from 'react-bootstrap'
import ProductCard from '../components/ProductCard'
import {useParams} from 'react-router-dom'

export default function OrderHistory(){

const {productId} = useParams()
const [name, setName] = useState('')
const [description, setDescription] = useState('')
const [quantity, setQuantity] = useState('')
const [price, setPrice]= useState('')
	
const [product, setProduct] = useState([])

useEffect(() => {
	fetch('http://localhost:4000/products/all')
	.then(res => res.json())
		.then(data => {

			//console.log(data)
			setName(data.name)
			setDescription(data.description)
			setQuantity(data.quantity)
			// setSubTotal(data.subTotal)
			// setTotalAmount(data.totalAmount)
		
		})
	}, [productId])


	return(


	<div>
	<h1> Order History </h1>


  <Table responsive="lg">
    <thead>
      <tr>
        <th>Item</th>
        <th>Product</th>
        <th>Description</th>
        <th>Price</th>
        <th>Quantity</th>
        <th>Subtotal</th>
        
      </tr>
    </thead>
    <tbody>
       <tr>
        <td>Order ID # 123456</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
     
      </tr>
      <tr>
        <td>1</td>
        <td>{description}</td>
        <td>Top</td>
        <td>300</td>
        <td>4</td>
        <td>1200</td>
     
      </tr>
      <tr>
        <td>2</td>
        <td>{product.description}</td>
        <td>Bottom</td>
        <td>900</td>
        <td>1</td>
        <td>900</td>
       
      </tr>
      <tr>
        <td>3</td>
        <td>Product 3</td>
        <td>Accessory</td>
        <td>500</td>
        <td>1</td>
        <td>500</td>
       
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <th>Total Price</th>
        <td> 2600</td>
        
      </tr>

    </tbody>
  </Table>
</div>
)}