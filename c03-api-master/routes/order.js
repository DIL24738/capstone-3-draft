const express = require("express");
const router = express.Router();
const orderController = require('../controllers/order');
const auth =  require('../auth')

//create Order
router.post('/order', auth.verify, (req, res) => {
	const userData =  auth.decode(req.headers.authorization)

  if(userData.isAdmin === false){
	orderController.createOrder(userData, req.params, req.body).then(resultFromController=> res.send(resultFromController))
  } else {
    res.send("not auth")
  }
})

//retrieve User Order
router.get("/myOrder", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  orderController.getOrder(userData).then(resultFromController => res.send(resultFromController));
})

//Update Order
router.put('/:orderId/update', auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  orderController.updateOrder(userData, req.params, req.body).then(resultFromController => res.send(resultFromController))
})


module.exports = router;
