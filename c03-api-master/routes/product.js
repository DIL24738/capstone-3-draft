const express = require('express');
const router = express.Router();
const productController = require('../controllers/product');
const auth = require('../auth')

//Add Product 
router.post('/addProduct', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin === true){
		productController.addProduct(req.body).then(resultFromController => res.send(resultFromController));
	} else {
		return res.send('No Access')
	}
});

// Retrieve all Products
router.get("/all" , (req,res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController))
});

// Retrieve available products
router.get("/", (req,res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController))
});

// Retrieve a specific product
router.get("/:productId", (req, res)=> {

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))
});

// Retrieve by Category
router.get('/categories/:category', (req, res)=> {
	productController.getCategory(req.params).then(resultFromController => res.send(resultFromController));
})

// Update Product
router.put('/:productId', auth.verify, (req, res)=> {
	const userData = auth.decode(req.headers.authorization);

	productController.updateProduct(userData, req.params, req.body).then(resultFromController => res.send(resultFromController));
})

// Archive Product
router.put('/:productId/archive', auth.verify, (req, res)=> {
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin === true){
		productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController));
	} else {
		return res.send('Not Authorized')
	}
} )

/*//add cart 
router.post('/:productId', auth.verify, (req, res)=> {
	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin === false){
		productController.addCart(userData, req.params, req.body).then(resultFromController => res.send(resultFromController))
	} else {
		return res.send('not auth')
	}
})
*/
module.exports = router;
