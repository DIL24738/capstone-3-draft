const express = require('express');
const router = express.Router();
const auth = require('../auth');
const userController = require('../controllers/user');

// Check Duplicate Email
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
});

//Register User
router.post('/register', (req, res)=> {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

//Authenticate User
router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

//Retrieve User Details
router.get("/details", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization)
	//console.log(userData)
	userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController))
})

// Admin setting a user into admin
router.put("/:userId/setAsAdmin", auth.verify, (req, res)=> {
	const userData = auth.decode(req.headers.authorization)
	// console.log(userData)
	 
	if(userData.isAdmin === true) {
		userController.setAsAdmin(req.params, req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send ('User not Authorized')
	}
})


// View Order
/*router.get('/:userId/order', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	userController.getOrder(userData, req.params).then(resultFromController => res.send(resultFromController))
})*/

// Retrieve All Order
/*router.get("/allOrder", auth.verify, (req, res)=> {
	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin === true){
		userController.getAllOrders().then(resultFromController => res.send(resultFromController))
	} else {
		res.send('User not Authorized')
	}
})

*/

// Get user's Order History

router.get('/myOrderHistory', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	userController.retrieveUserOrderHistory(userData.id).then(resultFromController => res.send(resultFromController))

})

// Show all items the user has added to their cart (and qty)


router.get('/myOrdersInCart', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	userController.retrieveUserOrdersInCart(userData.id).then(resultFromController => res.send(resultFromController))

})

module.exports = router;