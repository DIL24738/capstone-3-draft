const bcrypt = require('bcrypt');
const auth = require('../auth');
const Order = require('../models/Order')
const User = require('../models/User');
const Product = require('../models/Product');

// Check Duplicate Email
module.exports.checkEmailExists = (reqBody) => {
	
	return User.find({email: reqBody.email}).then(result => {

		if(result.length > 0) {
			return 'Email already exist.'
		} else {
			return false
		}
	}) 
}

//Register User
module.exports.registerUser = (reqBody) =>{

	let newUser = new User({
		name: reqBody.name,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		address: reqBody.address,
		mobileNo: reqBody.mobileNo
	})

	return newUser.save().then(()=> {
		return true
	}).catch(()=>{
		return false
	})
}

//Authenticate User
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result =>{
		if (result == null){
			return('Invalid Credentials')
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return ('Invalid Credentials')
			}
		}
	})
}

// Retrieve User Details
module.exports.getProfile = (data) => {
	
	console.log(data)
	
	return User.findById(data.userId).then(result => {
		
		result.password = "";
		return result;
	})
}

// Admin setting a user into admin
module.exports.setAsAdmin = (reqParams) => {

	let updatedIsAdmin = {
		isAdmin: true
	}

	return User.findByIdAndUpdate(reqParams.userId, updatedIsAdmin).then(()=> {
		return (`User is now an Admin`)
	}).catch(()=> {
		return false
	})
}


/*// View Order
module.exports.getOrder = async(userData, reqParams) => {

	if(userData.isAdmin === false && userData.id === reqParams.userId){
		return Order.find({customer: reqParams.userId}).populate('customer').populate({path: 'cartList', populate: 'product'}).sort({'purchasedOn': -1}).then(result => {
			if(result.length === 0){
				return ('Cart Empty')
			} else {
				return result
			}
		})
	} else {
		return ('Not Authorized')
	}
}

// Retrieve all Order
module.exports.getAllOrders = () => {
	return Order.find().populate('customer').populate({path: 'cartList', populate: 'product'}).sort({'purchasedOn': -1}).then(result => {
		return result
	})
}*/


// Get user's Order History

module.exports.retrieveUserOrderHistory = (userId) => {

	return Order.find({userId: userId, status: false}).then(result => {
		if (result == null) {
			return false
		} else {
			return result
		}
	})
}


// Show all items the user has added to their cart (and qty)

module.exports.retrieveUserOrdersInCart = (userId) => {

	return Order.find({userId: userId}).then(result => {
		if (result == null) {
			return false
		} else {
			return result
		}
	})
}