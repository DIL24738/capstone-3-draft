const bcrypt = require('bcrypt');
const auth = require('../auth');
const Order = require('../models/Order')
const User = require('../models/User');
const Product = require('../models/Product');


module.exports.createOrder = async (userData, reqParams, reqBody) =>{

	let {orderId, productId, quantity, userId, totalAmount, status, subTotal} = reqBody
	//console.log(userData)
	//let {productId} = reqParams
	//console.log(productId)
	//console.log(orderId)



	quantity = Number.parseInt(reqBody.quantity)

	try{
		let order = await Order.findOne({userId: userData.id});
		//console.log(order.status)
		//console.log(order)
		//console.log(Product.findById({_id: productId}))
		//console.log(productId)
		const productDetails = await Product.findById(productId)
		//console.log("productDetails", productDetails)
		console.log(productDetails) //result null

		if(order.status === true){
			//cart exists for user
			let cartListIndex = order.cartList.findIndex(p => p.productId == productId);
			//console.log(cartListIndex)
			//product exists in the cart, update the quantity
			if (cartListIndex > -1){
				let orderItem = order.cartList[cartListIndex];
				console.log(typeof(orderItem.quantity += quantity))
				console.log(productDetails.price)
				//console.log(typeof(orderItem.quantity))
				orderItem.quantity += quantity
				orderItem.subTotal = orderItem.quantity * productDetails.price
				order.cartList[cartListIndex] = orderItem;
				//order.totalAmount = //compute total amount
				 return await order.save().then(() => {
				 	return order
				 }).catch((err) => {
				 	return err
				 })
			} else {
				 //product does not exists in cart, add new item
				 order.cartList.push({productId, quantity, subTotal});
				 return await order.save().then(() => {
				 	return order
				 }).catch((err) => {
				 	return err
				 })
			}

			order = await order.save().then(()=> {
				return order; 
			}).catch((err)=> {
				return err
			})

		} else {
			//no cart for user, create new cart
			const newOrder = await new Order({
				userId,
				cartList: [{productId, quantity, subTotal}],
				totalAmount: parseInt(productDetails.price * quantity) //not sure
			});

			return await newOrder.save().then(()=> {
				return newOrder;
			}).catch((err)=> {
				return err
			})
		}
	} catch (err) {
		console.log(err)
		return err
	}


}

// Retrieve User Active Order
module.exports.getOrder = (userData) => {

	return Order.find({userId: userData.id, status: true}).then(result => {
		return result
	}).catch(() => {
		return false
	})
}

//Update Order -- not working
/*module.exports.updateOrder = async(userData, reqParams, reqBody) => {
	if(userData.isAdmin === false){
		const productDetails = await Product.findById(productId)	

		let updatedOrder = {
		
		}

		return await Order.findByIdAndUpdate(reqParams.orderId, updatedOrder).then(() => {
			return true
		}).catch(() => {
			return false
		})
	} else {
		return('not auth')
	}
}
*/




