const bcrypt = require('bcrypt');
const auth = require('../auth');
const Cart = require('../models/Cart');
const Order = require('../models/Order')
const User = require('../models/User');
const Product = require('../models/Product');

/*module.exports.createCart = async (data) => {

	let isUserUpdated = await User.findById(data.userId).then(user => {
		cart.save({cartUser: data.userId}).then(() => {
			return true
		}).catch(()=>{
			return false
		})
	})

	let isCartListUpdated = await Product.findById(data.productId).then(product => {
		cart..save({productId: data.productId}).then(() => {
			return true
		}).catch(()=> {
			return false
		})
	})

	if(isUserUpdated && isCartListUpdated){
		return true
	} else {
		return false
	}
}*/

//Create Cart
module.exports.createCart = async (userData, reqParams, reqBody) => {
 	
  if(userData._id !== Cart.find({cartUser})){
	let newCart = new Cart ({

		cartUser : userData._id,
		products: [
			productId: reqParams.productId,
			quantity: reqBody.quantity
		],
		cartAmount: reqBody.cartAmount
	})

	return await newCart.save.then(() => {
		return true
	}).catch(() => {
		return false
	})
	} else {
		let updatedCart = {
			products: [
				productId:reqParams.productId,
				quantity: reqBody.quantity
			],
			cartAmount: reqBody.cartAmount
		}
		return Cart.findById(reqBody.cartId, updatedCart).then((result)=> {
			Cart.products.push(result).then(()=> {
				return true
			})
		}).catch(() => {
			return false
		})
	} 
}