const Product = require('../models/Product');


//Add Product
module.exports.addProduct = (reqBody) => {

	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		category: reqBody.category,
		price: reqBody.price,
		img: reqBody.img,
		inStock: reqBody.inStock,
	})

	return newProduct.save().then(()=>{
		return ('Product added')
	}).catch(()=> {
		return false
	})
}

// Retrieve all Products
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	});
};

// Retrieve available products
module.exports.getAllActive = () => {

	return Product.find({isActive: true}).then(result => {
		return result;
	});
};

// Retrieve a specific product
module.exports.getProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(result => {

		let product = {
			name: result.name,
			description: result.description,
			category: result.category,
			price: result.price,
			inStock: result.inStock,
			img: result.img
		}
		return product
	})
}

// Retrieve by Category
module.exports.getCategory = (reqParams) => {
	return Product.find({category: reqParams.category}).then(result => {
		return result
	})
}

//Update Product 
module.exports.updateProduct = async(userData, reqParams, reqBody) => {
	if(userData.isAdmin === true){
		let updatedProduct = {
			name: reqBody.name,
			description: reqBody.description,
			category: reqBody.category,
			price: reqBody.price,
			inStock: reqBody.inStock,
			img: reqBody.img
		}
		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then(()=> {
			return (`product is updated`)
		}).catch(()=>{
			return false
		})
	} else {
		return ('Not Authorized')
	}
}

//Archive Product
module.exports.archiveProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(result=> {
		if(result.isActive === true){

			result.isActive = false
			return result.save().then(()=> {
				return (`${result.name} is archived`)
			}).catch(()=> {
				return false
			})
		} else {
			return (`${result.name} already archived`)
		}
	})
}
