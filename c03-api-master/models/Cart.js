const mongoose = require('mongoose')

const cartSchema = new mongoose.Schema({
	cartUser : {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User',
		required: true
	},
	
	products: [{
		product:{
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Product',
		required: true
	},

	quantity: {
		type: Number,
		default: 1
	}
	}],

	cartAmount: {
		type: Number,
		default: 0
	}

	 //made into array, added cartUser, cartAmount
});

module.exports = mongoose.model('Cart', cartSchema);