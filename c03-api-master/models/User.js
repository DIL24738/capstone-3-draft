const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
	name:{
		type: String, 
		required: [true, 'Pls enter name']
	},

	email: {
		type: String,
		required: [true, 'Pls enter email'],
		unique: true,
		match: /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/
    
	}, 

	password: {
		type: String,
		required: [true, 'Pls enter password']
	},

	address: {
		type: String,
		required: [true, 'Pls enter address']
	},

	mobileNo: {
		type: String,
		required: [true, 'Pls enter mobile number' ]
	},

	isAdmin: {
		type: Boolean,
		default: false
	}

});

module.exports = mongoose.model('User', userSchema)