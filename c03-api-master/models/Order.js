const mongoose = require('mongoose')

const orderSchema = new mongoose.Schema({

	userId: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User',
		required: true
	},
	
	cartList: [
		{
			productId: {
				type: mongoose.Schema.Types.ObjectId,
				ref: 'Product',
				required: true
			},

			quantity: {
				type: Number, 
				default: 1
			},

			subTotal: {
				type: Number,
				default: 0
			}
		}
	],

	totalAmount: {
		type: Number,
		default: 0
	},

	purchasedOn: {
		type: Date,
		default: Date.now
	},

	status: {
		type: Boolean,
		default: true
	}
});

module.exports = mongoose.model('Order', orderSchema)