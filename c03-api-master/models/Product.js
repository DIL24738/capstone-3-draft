const mongoose = require('mongoose')

const productSchema = new mongoose.Schema({

	name:{
		type: String,
		required: [true, 'enter product name'],
		unique: true
	},

	description: {
		type: String,
		required: [true, 'product description']
	},

	category: {
		type: String,
		required: [true, 'product category']
	},

	price: {
		type: Number,
		required: [true, 'product price']
	}, 

	inStock: {
		type: Number,
		default: 50
	},

	isActive: {
		type: Boolean,
		default: true
	},

	createdOn: {
		type: Date,
		default: Date.now
	},

	img: {
		type: Array
	}, 

	onSale: {
		type: Boolean,
		default: false
	}
})

module.exports = mongoose.model('Product', productSchema)